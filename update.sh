#!/usr/bin/bash

spectool -fg containers-common.spec

echo "Changing storage.conf..."
sed -i -e 's/^driver.*=.*/driver = "overlay"/' -e 's/^mountopt.*=.*/mountopt = "nodev,metacopy=on"/' \
        storage.conf

echo "Changing seccomp.json..."
[ `grep "keyctl" seccomp.json | wc -l` == 0 ] && sed -i '/\"kill\",/i \
                                "keyctl",' seccomp.json
sed -i '/\"socketcall\",/i \
                                "socket",' seccomp.json

echo "Changing registries.conf..."
sed -i 's/^#.*unqualified-search-registries.*=.*/unqualified-search-registries = ["registry.fedoraproject.org", "registry.access.redhat.com", "docker.io", "quay.io"]/g' \
        registries.conf

grep '\nshort-name-mode="enforcing"' registries.conf
if [[ $? == 1 ]]; then
    echo -e '\nshort-name-mode="enforcing"' >> registries.conf
fi

echo "Changing containers.conf..."
sed -i -e 's/^#.*log_driver.*=.*/log_driver = "journald"/' containers.conf

git checkout origin default-policy.json
